package model;

import javafx.beans.property.SimpleStringProperty;

/**
 * Defines a Course
 * 
 * @author Tyler Vega
 * @version 2/9/2019
 */
public class Course {
	private int id;
	private SimpleStringProperty courseNumber;
	private SimpleStringProperty name;
	private SimpleStringProperty section;
	private SimpleStringProperty semester;
	private Instructor instructor;

	/**
	 * Creates a Course object of specified course number, name, section, and
	 * instructor.
	 * 
	 * @precondition courseNumber != null, name != null, section != null, instructor
	 *               != null
	 * 
	 * @param courseNumber The course number.
	 * @param name         The name of the course.
	 * @param section      The section of the course.
	 * @param semester	   The semester the course was taken.
	 * @param instructor   The instructor for the course.
	 * 
	 * @postcondition A new course is created with courseNumber, name, section, and
	 *                instructor set.
	 */
	public Course(int id, String courseNumber, String name, String section, String semester, Instructor instructor) {
		this.setId(id);
		this.setCourseNumber(courseNumber);
		this.setName(name);
		this.setSection(section);
		this.setSemester(semester);
		this.setInstructor(instructor);
	}
	
	/**
	 * Returns the course's id in the database.
	 * 
	 * @return The id.
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Sets the course's id.
	 * 
	 * @param id The course's id in the database.
	 * 
	 * @postcondition The id is set.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Returns the course number.
	 * 
	 * @return The course number.
	 */
	public String getCourseNumber() {
		return this.courseNumber.get();
	}

	/**
	 * Sets the course number.
	 * 
	 * @precondition courseNumber != null
	 * 
	 * @param courseNumber The course number.
	 * 
	 * @postcondition The course number is set.
	 */
	public void setCourseNumber(String courseNumber) {
		if (courseNumber == null) {
			throw new IllegalArgumentException("Course number cannot be null");
		}
		this.courseNumber = new SimpleStringProperty(courseNumber);
	}

	/**
	 * Returns the course name.
	 * 
	 * @return The course name.
	 */
	public String getName() {
		return this.name.get();
	}

	/**
	 * Sets the course name.
	 * 
	 * @precondition name != null
	 * 
	 * @param courseNumber The course name.
	 * 
	 * @postcondition The course name is set.
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException("Course name cannot be null");
		}
		this.name = new SimpleStringProperty(name);
	}

	/**
	 * Returns the course section.
	 * 
	 * @return The course section.
	 */
	public String getSection() {
		return this.section.get();
	}

	/**
	 * Sets the course section.
	 * 
	 * @precondition section != null
	 * 
	 * @param courseNumber The course section.
	 * 
	 * @postcondition The course section is set.
	 */
	public void setSection(String section) {
		if (section == null) {
			throw new IllegalArgumentException("section cannot be null");
		}
		this.section = new SimpleStringProperty(section);
	}

	/**
	 * Returns the course's instructor.
	 * 
	 * @return The course's instructor.
	 */
	public Instructor getInstructor() {
		return this.instructor;
	}

	/**
	 * Sets the course's instructor.
	 * 
	 * @precondition instructor != null
	 * 
	 * @param courseNumber The course's instructor.
	 * 
	 * @postcondition The course's instructor is set.
	 */
	public void setInstructor(Instructor instructor) {
		if (instructor == null) {
			throw new IllegalArgumentException("Instructor cannot be null");
		}
		this.instructor = instructor;
	}
	
	/**
	 * Returns the course's semester.
	 * 
	 * @return The course's semester.
	 */
	public String getSemester() {
		return this.semester.get();
	}
	
	/**
	 * Sets the course's semester.
	 * 
	 * @precondition semester != null
	 * 
	 * @param courseNumber The course's semester.
	 * 
	 * @postcondition The course's semester is set.
	 */
	public void setSemester(String semester) {
		if (semester == null) {
			throw new IllegalArgumentException("semster cannot be null");
		}
		this.semester = new SimpleStringProperty(semester);
	}

	/**
	 * Returns the string representation of the course by number, name, and section.
	 * 
	 * @return The string representation of the course.
	 */
	@Override
	public String toString() {
		return this.courseNumber.get() + " " + this.name.get() + " - " + this.section.get();
	}

}
