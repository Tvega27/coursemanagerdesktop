package model;

import javafx.beans.property.SimpleDoubleProperty;

/**
 * Defines a GradeItem.
 * 
 * @author Tyler Vega
 * @version 2/9/2019
 */
public class GradeItem {
	private int id;
	private String category;
	private String title;
	private String description;
	private Student student;
	private SimpleDoubleProperty grade;
	private Course course;
	private SimpleDoubleProperty maxGrade;

	/**
	 * Creates a GradeItem of specified category, title, description, student,
	 * grade, and course.
	 * 
	 * @precondition category != null
	 * @precondition title != null
	 * @precondition description != null
	 * @precondition student != null
	 * @precondition grade != null
	 * @precondition course != null
	 * 
	 * @param id          The id in the database.
	 * @param category    The category.
	 * @param title       The title.
	 * @param description The description.
	 * @param student     The student.
	 * @param grade       The grade.
	 * @param course      The course associated.
	 * @param maxGrade    The max grade the grade item can be.
	 * 
	 * @postcondition A new GradeItem is made with category, title, description,
	 *                student, grade, and course set.
	 */
	public GradeItem(int id, String category, String title, String description, Student student, double grade,
			Course course, double maxGrade) {
		this.setId(id);
		this.setCategory(category);
		this.setTitle(title);
		this.setDescription(description);
		this.setStudent(student);
		this.setGrade(grade);
		this.setCourse(course);
		this.setMaxGrade(maxGrade);
	}

	/**
	 * Returns the id of the grade item in the database.
	 * 
	 * @return The id of the grade item.
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Sets the id of the grade item.
	 * 
	 * @param id The id.
	 * 
	 * @postcondition the id is set.
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Returns the category.
	 * 
	 * @return The category.
	 */
	public String getCategory() {
		return this.category;
	}

	/**
	 * Sets the category.
	 * 
	 * @precondition category != null
	 * 
	 * @param category The grade item's category.
	 * 
	 * @postcondition The category is set.
	 */
	public void setCategory(String category) {
		if (category == null) {
			throw new IllegalArgumentException("category cannot be null");
		}
		this.category = category;
	}

	/**
	 * Returns the Title.
	 * 
	 * @return The title.
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Sets the title.
	 * 
	 * @precondition title != null
	 * 
	 * @param title The grade item's title.
	 * 
	 * @postcondition The title is set.
	 */
	public void setTitle(String title) {
		if (title == null) {
			throw new IllegalArgumentException("title cannot be null");
		}
		this.title = title;
	}

	/**
	 * Returns the section.
	 * 
	 * @return The section.
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Sets the description.
	 * 
	 * @precondition description != null
	 * 
	 * @param section The grade item's description.
	 * 
	 * @postcondition The description is set.
	 */
	public void setDescription(String section) {
		if (section == null) {
			throw new IllegalArgumentException("section cannot be null");
		}
		this.description = section;
	}

	/**
	 * Returns the student.
	 * 
	 * @return The student.
	 */
	public Student getStudent() {
		return this.student;
	}

	/**
	 * Sets the student.
	 * 
	 * @precondition student != null
	 * 
	 * @param student The grade item's student.
	 * 
	 * @postcondition The student is set.
	 */
	public void setStudent(Student student) {
		if (student == null) {
			throw new IllegalArgumentException("student cannot be null");
		}
		this.student = student;
	}

	/**
	 * Returns the grade.
	 * 
	 * @return The grade.
	 */
	public double getGrade() {
		return this.grade.get();
	}

	/**
	 * Sets the grade.
	 * 
	 * @precondition grade >= 0.0
	 * 
	 * @param grade The grade item's grade.
	 * 
	 * @postcondition The grade is set.
	 */
	public void setGrade(double grade) {
		if (grade < 0.0) {
			throw new IllegalArgumentException("grade cannot be negative");
		}
		this.grade = new SimpleDoubleProperty(grade);
	}

	/**
	 * Returns the course.
	 * 
	 * @return The course.
	 */
	public Course getCourse() {
		return this.course;
	}

	/**
	 * Sets the course.
	 * 
	 * @precondition course != null
	 * 
	 * @param course The grade item's course.
	 * 
	 * @postcondition The course is set.
	 */
	public void setCourse(Course course) {
		if (course == null) {
			throw new IllegalArgumentException("course cannot be null");
		}
		this.course = course;
	}

	/**
	 * Returns the max grade.
	 * 
	 * @return the max grade.
	 */
	public double getMaxGrade() {
		return this.maxGrade.get();
	}

	/**
	 * Sets the max grade of the grade item.
	 * 
	 * @precondition grade >= 0
	 * 
	 * @param course The grade item's max grade.
	 * 
	 * @postcondition The max grade is set.
	 */
	public void setMaxGrade(double grade) {
		if (grade < 0) {
			throw new IllegalArgumentException("max grade cannot be negative");
		}
		this.maxGrade = new SimpleDoubleProperty(grade);
	}

}
