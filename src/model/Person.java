package model;
/**
 * Defines a Person
 * 
 * @author Tyler Vega
 * @version 2/8/2019
 */
public class Person {
	private int id;
	private String firstName;
	private String lastName;
	private String email;

	public Person() {

	}

	/**
	 * Constructs a new Person of specified first and last names and email.
	 * 
	 * @precondition none
	 * 
	 * @param id The person's id in the database.
	 * 
	 * @param firstName The first name of the Person.
	 * 
	 * @param lastName  The last name of the Person.
	 * 
	 * @param email     The email of the Person.
	 * 
	 * @postcondition a new Person object is made.
	 */
	public Person(int id, String firstName, String lastName, String email) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}
	
	/**
	 * Returns the id of the person in the database.
	 * 
	 * @return The id.
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * Returns the first name.
	 * 
	 * @return The first name.
	 */
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Returns the last name.
	 * 
	 * @return The last name.
	 */
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Returns the email.
	 * 
	 * @return The email.
	 */
	public String getEmail() {
		return this.email;
	}
	
	
}
