package model;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * Defines a RubricItem
 * 
 * @author Tyler Vega
 * @version 2/12/2019
 */
public class RubricItem {
	private int id;
	private SimpleStringProperty title;
	private SimpleDoubleProperty weight;

	/**
	 * Creates a RubricItem of specified title and weight.
	 * 
	 * @precondition title != null
	 * @precondition weight > 0.0 && weight < 1.0
	 * 
	 * @param id 	 The id in the database
	 * @param title  The title of the item.
	 * @param weight The weight of the item between 0.0 and 1.0.
	 * 
	 * @postcondition The RubricItem is made with the set title and weight.
	 */
	public RubricItem(int id, String title, double weight) {
		this.setId(id);
		this.setTitle(title);
		this.setWeight(weight);
	}
	
	/**
	 * Sets the RubricItem's id
	 * 
	 * @param id 	the id
	 * 
	 * @postcondition the id is set
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * Gets the id of the RubricItem
	 * 
	 * @return the id of the RubricItem
	 */
	public int getId() {
		return this.id;
	}

	/**
	 * Returns the title.
	 * 
	 * @return The title.
	 */
	public String getTitle() {
		return this.title.get();
	}

	/**
	 * Sets the title.
	 * 
	 * @precondition title != null
	 * 
	 * @param title The title for the item.
	 * 
	 * @postcondition The title is set.
	 */
	public void setTitle(String title) {
		if (title == null) {
			throw new IllegalArgumentException("Title cannot be null.");
		}
		this.title = new SimpleStringProperty(title);
	}

	/**
	 * Returns the weight.
	 * 
	 * @return The weight.
	 */
	public double getWeight() {
		return this.weight.get();
	}

	/**
	 * Sets the weight.
	 * 
	 * @precondition weight > 0.0 && weight < 1.0
	 * 
	 * @param weight The weight of the item between 0.0 and 1.0.
	 * 
	 * @postcondition The weight is set.
	 */
	public void setWeight(double weight) {
		if (weight < 0.0 || weight > 1.0) {
			throw new IllegalArgumentException("Weight must be between 0.0 and 1.0.");
		}
		this.weight = new SimpleDoubleProperty(weight);
	}

}
