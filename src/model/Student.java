package model;

/**
 * Defines a Student, subclass of Person
 * 
 * @author Tyler Vega
 * @version 2/8/2019
 */
public class Student extends Person {
	private String degreeProgram;

	/**
	 * Creates a new Student object of specified first and last name, email, and
	 * degree program.
	 * 
	 * @precondition degreeProgram != null
	 * 
	 * @param id            The student's id in the database.
	 * 
	 * @param firstName     The student's first name.
	 * 
	 * @param lastName      The student's last name.
	 * 
	 * @param email         The student's email.
	 * 
	 * @param degreeProgram The student's degree program.
	 * 
	 * @postcondition A new Student object is made.
	 */
	public Student(int id, String firstName, String lastName, String email, String degreeProgram) {
		super(id, firstName, lastName, email);

		this.setDegreeProgram(degreeProgram);
	}

	/**
	 * Returns the student's degreeProgram
	 * 
	 * @return The degree program.
	 */
	public String getDegreeProgram() {
		return degreeProgram;
	}

	/**
	 * Sets the student's degreeProgram
	 * 
	 * @precondition degreeProgram != null
	 * 
	 * @param degreeProgram The degree program to set.
	 * 
	 * @postcondition The degree program is set.
	 */
	public void setDegreeProgram(String degreeProgram) {
		if (degreeProgram == null) {
			throw new IllegalArgumentException("Degree Program cannot be null");
		}
		this.degreeProgram = degreeProgram;
	}

	/**
	 * Returns a string representation of the student by name.
	 * 
	 * @return a string of the student by name.
	 */
	@Override
	public String toString() {
		return this.getFirstName() + " " + this.getLastName();
	}

}
