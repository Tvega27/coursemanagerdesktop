package model;

import java.util.ArrayList;

public class SummaryInfo {
	
	private Student student;
	private ArrayList<GradeItem> gradeItems;
	private double courseTotal;
	
	public SummaryInfo(Student student, ArrayList<GradeItem> gradeItems) {
		this.student = student;
		this.gradeItems = gradeItems;
		this.setCourseTotal(0.0);
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ArrayList<GradeItem> getGradeItems() {
		return this.gradeItems;
	}

	public void setGradeItems(ArrayList<GradeItem> gradeItems) {
		this.gradeItems = gradeItems;
	}

	public double getCourseTotal() {
		return this.courseTotal;
	}

	public void setCourseTotal(double courseTotal) {
		this.courseTotal = courseTotal;
	}
	
	public void calculateGradeForCategory(String category, double weight) {
		double catGrade = 0.0;
		double totalGrade = this.courseTotal;
		double catSum = 0.0;
		double catCount = 0.0;
		for(var item : this.gradeItems) {
			if(item.getCategory().equals(category)) {
				catSum += (item.getGrade() / item.getMaxGrade()) * 100;
				catCount++;
			}
		}
		
		if(catCount != 0) {
			catGrade = ((catSum/catCount) * weight);
			totalGrade += catGrade;
			this.setCourseTotal(totalGrade);
		}
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.student.getFirstName() + " " + this.student.getLastName() + "   \t\t\t");
		for(var item : this.gradeItems) {
			sb.append(item.getTitle() + ":   \t" + item.getGrade() + "   \t");
		}
		sb.append("Overall Grade:   \t" + this.getCourseTotal());
		return sb.toString();
	}

}
