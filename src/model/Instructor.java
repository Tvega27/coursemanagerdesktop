package model;

/**
 * Defines an Instructor, subclass of Person
 * 
 * @author Tyler Vega
 * @version 2/9/2019
 */
public class Instructor extends Person {
	private String publicEmail;

	/**
	 * Creates a new Instructor object of specified first and last name, email, and
	 * public email.
	 * 
	 * @param id          The instructor's id in the database.
	 * 
	 * @param firstName   The instructor's first name.
	 * 
	 * @param lastName    The instructor's last name.
	 * 
	 * @param email       The instructor's email.
	 * 
	 * @param publicEmail The instructor's public email.
	 * 
	 * @postcondition A new Instructor object is made.
	 */
	public Instructor(int id, String firstName, String lastName, String email, String publicEmail) {
		super(id, firstName, lastName, email);
		this.publicEmail = publicEmail;
	}

	/**
	 * Returns the instructor's public email.
	 * 
	 * @return The instructor's public email.
	 */
	public String getPublicEmail() {
		return publicEmail;
	}

	/**
	 * Sets the instructor's public email
	 * 
	 * @param publicEmail The email to set as public email.
	 * 
	 * @postcondition The public email is set.
	 */
	public void setPublicEmail(String publicEmail) {
		this.publicEmail = publicEmail;
	}

}
