package controller;

import java.io.IOException;
import java.sql.SQLException;

import dal.SQLLoginDal;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;

public class FirstTimeLoginViewController {

  @FXML
  private TextField usernameBox;

  @FXML
  private PasswordField passwordBox;

  @FXML
  private PasswordField reEnterPasswordBox;

  @FXML
  private Button submitBtn;

  @FXML
  private Button cancelBtn;

  private Stage stage;
  private SQLLoginDal loginDal = new SQLLoginDal();

  public FirstTimeLoginViewController() {
    this.stage = new Stage();
    try {
      FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/FirstTimeLoginView.fxml"));
      loader.setController(this);
      stage.setScene(new Scene(loader.load()));
      stage.setTitle("Course Manager Login Setup");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void showStage() {
    this.stage.show();
  }

  @FXML
  private void cancelClicked(ActionEvent event) {
    this.loadNewStage();
  }

  @FXML
  private void passwordEntered(ActionEvent event) {
    this.reEnterPasswordBox.setDisable(false);
  }

  @FXML
  private void passwordReEntered(ActionEvent event) {
    if (this.passwordBox.getText().equals(this.reEnterPasswordBox.getText())) {
      this.submitBtn.setDisable(false);
    } else {
      this.throwAlertMessage("Passwords Do Not Match",
          "Re-enter the passwords to make sure they match.");
    }
  }

  @FXML
  private void submitCreationClick(ActionEvent event) {
    try {
      this.loginDal.setPassword(this.usernameBox.getText(), this.passwordBox.getText());
      this.loadNewStage();
    } catch (SQLException e) {
      this.throwAlertMessage("Failed", "Password creation failed.");
      e.printStackTrace();
    }
  }

  @FXML
  private void usernameEntered(ActionEvent event) {
    String username = this.usernameBox.getText();
    if (username != null) {
      try {
        if (this.loginDal.checkAuthUsername(username)) {
          this.passwordBox.setDisable(false);
        }

      } catch (SQLException e) {

        e.printStackTrace();
      }
    }
  }

  private void loadNewStage() {
    LoginViewController controller = new LoginViewController();
    this.stage.close();
    controller.showStage();
  }

  private void throwAlertMessage(String title, String message) {
    Alert alert = new Alert(AlertType.INFORMATION);
    alert.setTitle(title);
    alert.setHeaderText(title);
    alert.setContentText(message);
    alert.show();
  }
}
