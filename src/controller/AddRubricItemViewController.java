package controller;

import java.sql.SQLException;

import dal.SQLRubricItemDal;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Course;
import model.RubricItem;

public class AddRubricItemViewController {
  private Course course;
  
  @FXML
  private TextField titleTextArea;

  @FXML
  private TextField weightTxtArea;
  
  @FXML
  private Button addBtn;

  @FXML
  private Button cancelBtn;

  public AddRubricItemViewController(Course course) {
    this.course = course;
  }
  
  @FXML
  void onAddClick(ActionEvent event) {
    String title = this.titleTextArea.getText();
    if (title.equals("")) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setContentText("Title cannot be empty");
      alert.showAndWait();
      return;
    }
    Double weight = null;
    try {
      weight = Double.parseDouble(this.weightTxtArea.getText());
    } catch (NumberFormatException e) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setContentText(weight + " is not a valid number.");
      alert.showAndWait();
      return;
    }
    
    if (weight < 0.0 || weight > 1.0) {
      Alert alert = new Alert(AlertType.ERROR);
      alert.setContentText("Weight must be between 0.0 and 1.0");
      alert.showAndWait();
      return;
    }
    
    RubricItem rubricItem = new RubricItem(this.course.getId(), title, weight);
    var rubricItemDAL = new SQLRubricItemDal();
    
    try {
      rubricItemDAL.addRubricItem(rubricItem);
    } catch (SQLException e) {
      e.printStackTrace();
    }
    Stage stage = (Stage) this.addBtn.getScene().getWindow();
    stage.close();
  }

  @FXML
  void onCancelClick(ActionEvent event) {
    Stage stage = (Stage) this.cancelBtn.getScene().getWindow();
    stage.close();
  }
  
}
