package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import dal.SQLCourseDal;
import dal.SQLGradeItemDal;
import dal.SQLRubricItemDal;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Accordion;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.util.converter.DoubleStringConverter;
import model.Course;
import model.GradeItem;
import model.Instructor;
import model.RubricItem;
import model.Student;
import model.SummaryInfo;

/**
 * Controller for the MainView
 * 
 * @author Tyler Vega
 * @version 2/8/2019
 */

public class MainViewController {
	@FXML
	private AnchorPane anchor;

	@FXML
	private Accordion contentAccordian;

	@FXML
	private MenuBar mainViewMenuBar;

	@FXML
	private TitledPane courseManagementTitlePane;

	@FXML
	private TableView<RubricItem> rubricTable;

	@FXML
	private TableColumn<RubricItem, String> rubricTitleCol;

	@FXML
	private TableColumn<RubricItem, Double> rubricWeightCol;

	@FXML
	private TitledPane gradeManagementTitlePane;

	@FXML
	private ComboBox<String> gmAssignmentComboBox;

	@FXML
	private ComboBox<String> gmCategoryComboBox;

	@FXML
	private TableView<GradeItem> gradeManagementTable;

	@FXML
	private TableColumn<GradeItem, Student> gmStudentCol;

	@FXML
	private TableColumn<GradeItem, Double> gmGradeCol;

	@FXML
	private ListView<Course> courseListView;

	@FXML
	private ComboBox<String> courseSemesterComboBox;

	@FXML
	private Label gmFeedbackLbl;

	@FXML
	private Button addAssignmentBtn;

	@FXML
	private Button addRubricItemBtn;

	@FXML
	private TextField assignmentTitleTxtField;

	@FXML
	private TextArea gmAssignmentDescription;

	@FXML
	private TextField maxGradeBox;

	@FXML
	private ComboBox<String> cmAssignmentComboBox;

	@FXML
	private ComboBox<String> cmCategoryComboBox;

	@FXML
	private RadioButton cmEditGradeItemRadioBtn;

	@FXML
	private Button updateGradeItemBtn;

	@FXML
	private Button deleteAssignmentBtn;

	@FXML
	private Button deleteRubricItemBtn;

	@FXML
	private Button warningOkBtn;

	@FXML
	private Label rubricItemWarningLbl;
	
	@FXML
	private TitledPane courseSummaryTitlePane;
	
	@FXML
	private ListView<SummaryInfo> summaryTable;

	private Stage stage;
	private Instructor instructor;
	private Course selectedCourse;
	private ArrayList<String> categories;
	private ArrayList<String> assignments;

	private SQLCourseDal courseDal;
	private SQLRubricItemDal rubricItemDal;
	private SQLGradeItemDal gradeItemDal;

	private ObservableList<Course> courses;
	private ObservableList<RubricItem> rubricItems;

	public MainViewController(Instructor instructor) {
		this.stage = new Stage();
		this.setInstructor(instructor);

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/MainView.fxml"));
			loader.setController(this);
			this.stage.setScene(new Scene(loader.load()));
			this.stage.setTitle("Course Manager: Logged in as " + this.instructor.getFirstName() + " "
					+ this.instructor.getLastName());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void showStage() {
		this.stage.show();
	}

	@FXML
	private void initialize() {
		this.selectedCourse = null;
		this.courseDal = new SQLCourseDal();
		this.rubricItemDal = new SQLRubricItemDal();
		this.gradeItemDal = new SQLGradeItemDal();
		this.courses = FXCollections.observableArrayList(new ArrayList<Course>());
		this.contentAccordian.setExpandedPane(this.courseManagementTitlePane);

		this.loadListView();
		this.loadSemesterComboBox();
		this.initializeTableViews();
	}

	public void setInstructor(Instructor instructor) {
		if (instructor == null) {
			throw new IllegalArgumentException("Instructor cannot be null.");
		}
		this.instructor = instructor;
	}

	private void loadListView() {

		try {
			this.courses = FXCollections.observableArrayList(this.courseDal.getCoursesByInstructor(this.instructor));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.courseListView.setItems(this.courses);

	}

	private void initializeTableViews() {
		this.rubricTable.setEditable(true);
		this.rubricTitleCol.setCellValueFactory(new PropertyValueFactory<RubricItem, String>("title"));
		this.rubricWeightCol.setCellValueFactory(new PropertyValueFactory<RubricItem, Double>("weight"));
		this.gmStudentCol.setCellValueFactory(new PropertyValueFactory<GradeItem, Student>("student"));
		this.gmGradeCol.setCellValueFactory(new PropertyValueFactory<GradeItem, Double>("grade"));
		this.rubricTitleCol.setCellFactory(TextFieldTableCell.<RubricItem>forTableColumn());
		this.rubricWeightCol
				.setCellFactory(TextFieldTableCell.<RubricItem, Double>forTableColumn(new DoubleStringConverter()));
		this.gmGradeCol
				.setCellFactory(TextFieldTableCell.<GradeItem, Double>forTableColumn(new DoubleStringConverter()));

		this.gmGradeCol.setOnEditCommit(new EventHandler<CellEditEvent<GradeItem, Double>>() {

			@Override
			public void handle(CellEditEvent<GradeItem, Double> grade) {
				((GradeItem) grade.getTableView().getItems().get(grade.getTablePosition().getRow()))
						.setGrade(grade.getNewValue());
				updateGradeItem();

			}
		});

		this.rubricTitleCol.setOnEditCommit(new EventHandler<CellEditEvent<RubricItem, String>>() {

			@Override
			public void handle(CellEditEvent<RubricItem, String> title) {
				((RubricItem) title.getTableView().getItems().get(title.getTablePosition().getRow()))
						.setTitle(title.getNewValue());
				updateRubricItem();

			}
		});

		this.rubricWeightCol.setOnEditCommit(new EventHandler<CellEditEvent<RubricItem, Double>>() {

			@Override
			public void handle(CellEditEvent<RubricItem, Double> weight) {
				((RubricItem) weight.getTableView().getItems().get(weight.getTablePosition().getRow()))
						.setWeight(weight.getNewValue());
				updateRubricItem();

			}
		});
	}
	
	private void populateSummaryTable() {
		try {
			ArrayList<SummaryInfo> summary = new ArrayList<SummaryInfo>();
			ArrayList<GradeItem> items = new ArrayList<GradeItem>();
			for(var student : this.courseDal.getStudentsInCourse(this.selectedCourse)) {
				items = this.gradeItemDal.getGradeItemsForStudentInCourse(this.selectedCourse, student);
				SummaryInfo info = new SummaryInfo(student, items);
				for(var category : this.rubricItems) {
					info.calculateGradeForCategory(category.getTitle(), category.getWeight());
				}
				summary.add(info);
				
			}
			this.setUpSummaryTable(summary);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setUpSummaryTable(ArrayList<SummaryInfo> summaryList) {
		this.summaryTable.setItems(FXCollections.observableArrayList(summaryList));
	}

	private void loadSemesterComboBox() {
		ArrayList<String> semesters = new ArrayList<String>();
		semesters.add("All");
		for (var course : this.courses) {
			if (!semesters.contains(course.getSemester())) {
				semesters.add(course.getSemester());
			}
		}
		this.courseSemesterComboBox.setItems(FXCollections.observableArrayList(semesters));
	}

	@FXML
	private void courseListViewMouseClick(MouseEvent event) {
		this.selectedCourse = this.courseListView.getSelectionModel().getSelectedItem();
		this.updateRubricTable();
		this.updateEditGradeItemUI();
		this.updateGradeManagementTable();
		this.resetGradeManagementUI();
		this.populateSummaryTable();
	}

	private void resetGradeManagementUI() {
		this.gmAssignmentComboBox.setDisable(true);

		this.gradeManagementTable.setDisable(false);
		this.gmCategoryComboBox.getSelectionModel().clearSelection();
		this.cmCategoryComboBox.getSelectionModel().clearSelection();
		this.gmAssignmentComboBox.getSelectionModel().clearSelection();
		this.cmAssignmentComboBox.getSelectionModel().clearSelection();
		this.gradeManagementTable.getItems().clear();
		this.cmAssignmentComboBox.setDisable(true);
		this.gmAssignmentDescription.setText("");
		this.assignmentTitleTxtField.setText("");
		this.maxGradeBox.setText("");
	}

	private void updateRubricTable() {

		try {
			this.rubricItems = FXCollections
					.observableArrayList(this.rubricItemDal.getRubricItemsByRubric(this.selectedCourse));
			this.rubricTable.setItems(this.rubricItems);
			
			double sum = 0;
			for(var item : this.rubricItems) {
				sum += item.getWeight();
			}
			
			if(sum != 1.0) {
				this.showRubricWarning(true);
			} else {
				this.showRubricWarning(false);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	private void showRubricWarning(boolean value) {
		this.warningOkBtn.setVisible(value);
		this.rubricItemWarningLbl.setVisible(value);
	}

	private void updateGradeManagementTable() {
		this.categories = new ArrayList<String>();
		for (var item : this.rubricItems) {
			this.categories.add(item.getTitle());
		}
		ObservableList<String> categoryList = FXCollections.observableArrayList(this.categories);
		this.gmCategoryComboBox.setItems(categoryList);
	}

	private void updateEditGradeItemUI() {
		this.categories = new ArrayList<String>();
		for (var item : this.rubricItems) {
			this.categories.add(item.getTitle());
		}
		ObservableList<String> categoryList = FXCollections.observableArrayList(this.categories);
		this.cmCategoryComboBox.setItems(categoryList);
	}

	private void populateGradeItemUI() {
		String category = this.cmCategoryComboBox.getValue();
		String assignment = this.cmAssignmentComboBox.getValue();

		try {
			if (category != null && assignment != null) {
				ObservableList<GradeItem> items = FXCollections.observableArrayList(
						this.gradeItemDal.getGradeItemsByCategoryAssignment(this.selectedCourse, category, assignment));
				if (this.cmEditGradeItemRadioBtn.isSelected()) {
					this.gmAssignmentDescription.setText(items.get(0).getDescription());
					this.maxGradeBox.setText(String.valueOf(items.get(0).getMaxGrade()));
					this.assignmentTitleTxtField.setText(String.valueOf(items.get(0).getTitle()));
				}

			}
		} catch (SQLException e) {
			this.throwAlertMessage("Failure", "Something went wrong populating the edit grade item panel.");
		}

	}
	
	private void populateGradeManagementUI() {
		String category = this.gmCategoryComboBox.getValue();
		String assignment = this.gmAssignmentComboBox.getValue();
		if (category != null && assignment != null) {
			try {
				ObservableList<GradeItem> items = FXCollections.observableArrayList(
						this.gradeItemDal.getGradeItemsByCategoryAssignment(this.selectedCourse, category, assignment));
				this.gradeManagementTable.setItems(items);
			} catch(SQLException e) {
				this.throwAlertMessage("Failure", "Something went wrong populating the grade management table.");
			}


		}
	}

	@FXML
	private void editGradeItemSelected(ActionEvent event) {
		if (this.selectedCourse != null) {
			if (this.cmEditGradeItemRadioBtn.isSelected()) {
				this.updateEditGradeItemUI();
				this.addAssignmentBtn.setDisable(true);
				this.updateGradeItemBtn.setDisable(false);
				this.deleteAssignmentBtn.setDisable(false);
			} else {
				this.resetGradeManagementUI();
				this.addAssignmentBtn.setDisable(false);
				this.updateGradeItemBtn.setDisable(true);
				this.deleteAssignmentBtn.setDisable(true);
			}
		}

	}

	@FXML
	private void gmCategorySelected(ActionEvent event) {
		this.gradeManagementTable.getItems().clear();
		String category = this.gmCategoryComboBox.getValue();
		if (category != null) {
			this.gmAssignmentComboBox.setDisable(false);
			this.populateAssignmentTitles(category);
			this.gmAssignmentComboBox.setItems(FXCollections.observableArrayList(this.assignments));
		}
	}

	@FXML
	private void cmCategorySelected(ActionEvent event) {
		String category = this.cmCategoryComboBox.getValue();
		if (this.cmEditGradeItemRadioBtn.isSelected()) {
			if (category != null) {
				this.cmAssignmentComboBox.setDisable(false);
				this.populateAssignmentTitles(category);
				this.cmAssignmentComboBox.setItems(FXCollections.observableArrayList(this.assignments));
			}

		}
	}

	private void populateAssignmentTitles(String category) {
		try {
			this.assignments = this.gradeItemDal.getAssignmentTitles(this.selectedCourse, category);
		} catch (SQLException e) {
			this.throwAlertMessage("Failure", "Something went wrong populating the assignment dropdown box.");
		}
	}

	@FXML
	private void cmAssignmentSelected(ActionEvent event) {
		this.populateGradeItemUI();
	}

	@FXML
	private void gmAssignmentSelected(ActionEvent event) {
		this.populateGradeManagementUI();
	}

	@FXML
	private void courseSemesterSelected(ActionEvent event) {
		String selectedSemester = this.courseSemesterComboBox.getValue();
		ArrayList<Course> courses = new ArrayList<Course>();
		if (selectedSemester != null) {
			if (selectedSemester.equals("All")) {
				this.courseListView.setItems(FXCollections.observableArrayList(this.courses));
			} else {
				for (var course : this.courses) {
					if (course.getSemester().equals(selectedSemester)) {
						courses.add(course);
					}
				}
				this.courseListView.setItems(FXCollections.observableArrayList(courses));
			}
		}
	}

	private void updateGradeItem() {
		GradeItem item = this.gradeManagementTable.getSelectionModel().getSelectedItem();
		if (item.getGrade() <= item.getMaxGrade()) {
			try {
				this.gradeItemDal.updateGradeItemGrade(item);
				String message = "Grade Item for " + item.getStudent().getFirstName() + " "
						+ item.getStudent().getLastName() + " Updated Successfully";
				this.throwAlertMessage("Success", message);
			} catch (SQLException e) {

				this.throwAlertMessage("Failure", "Grade Item Failed to Update");
				e.printStackTrace();
			}
		} else {
			this.throwAlertMessage("Failure",
					"Grade must be below max grade and positive.\nThe item's grade was not set, please change the\ngrade to update the database.");
		}

	}

	@FXML
	private void deleteAssignment(ActionEvent event) {
		String category = this.cmCategoryComboBox.getValue();
		String assignment = this.cmAssignmentComboBox.getValue();
		
		if(category != null && assignment != null) {
			try {
				this.gradeItemDal.deleteGradeItem(this.selectedCourse.getId(), category, assignment);
				this.resetGradeManagementUI();
				this.updateEditGradeItemUI();
				this.throwAlertMessage("Success", "Successfully deleted the grade item.");
			} catch (SQLException e) {
				this.throwAlertMessage("Failure", "Something went wrong deleting the grade item.");
				e.printStackTrace();
			}
		}
		
	}

	@FXML
	private void deleteRubricItem(ActionEvent event) {
		RubricItem item = this.rubricTable.getSelectionModel().getSelectedItem();
		if (item != null) {
			try {
				this.rubricItemDal.deleteRubricItem(item);
				this.updateRubricTable();
			} catch (SQLException e) {
				this.throwAlertMessage("Failure", "Something went wrong deleting the rubric item.");
			}
		}

	}

	private void throwAlertMessage(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(title);
		alert.setContentText(message);
		alert.show();
	}

	private void updateRubricItem() {
		RubricItem item = this.rubricTable.getSelectionModel().getSelectedItem();
		try {
			this.rubricItemDal.updateRubricItem(item);
			this.updateRubricTable();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.updateRubricTable();
	}

	@FXML
	private void addAssignment(ActionEvent event) {
		String assignment = this.assignmentTitleTxtField.getText();
		String category = this.cmCategoryComboBox.getSelectionModel().getSelectedItem();
		String description = this.gmAssignmentDescription.getText();
		double maxGrade = Double.parseDouble(this.maxGradeBox.getText());
		try {

			ArrayList<Student> roster = this.courseDal.getStudentsInCourse(this.selectedCourse);

			for (var student : roster) {
				GradeItem item = new GradeItem(0, category, assignment, description, student, 0.0, this.selectedCourse,
						maxGrade);
				this.gradeItemDal.addGradeItem(item);
			}
			this.resetGradeManagementUI();
			this.throwAlertMessage("Success", "The new assignment was successfully added to " + this.selectedCourse);
		} catch (SQLException e) {
			this.throwAlertMessage("Something Went Wrong",
					"Something went wrong adding the assignment. Make sure all fields are entered correctly.");
		}
	}

	@FXML
	private void updateGradeItems(ActionEvent event) {
		String assignment = this.assignmentTitleTxtField.getText();
		String category = this.cmCategoryComboBox.getSelectionModel().getSelectedItem();
		String description = this.gmAssignmentDescription.getText();
		double maxGrade = Double.parseDouble(this.maxGradeBox.getText());
		try {

			ArrayList<Student> roster = this.courseDal.getStudentsInCourse(this.selectedCourse);

			for (var student : roster) {
				GradeItem item = new GradeItem(0, category, assignment, description, student, 0.0, this.selectedCourse,
						maxGrade);
				this.gradeItemDal.updateGradeItem(item);
			}
			this.resetGradeManagementUI();
			this.throwAlertMessage("Success", "The new assignment was successfully updated ");
		} catch (SQLException e) {
			this.throwAlertMessage("Something Went Wrong",
					"Something went wrong updating the assignment. Make sure all fields are entered correctly.");
		}
	}

	@FXML
	void updateGradeItemDescription(MouseEvent event) {

	}

	@FXML
	void onAddRubricItemClick(ActionEvent event) {
		var controller = new AddRubricItemViewController(this.selectedCourse);
		this.stage = new Stage();

		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/AddRubricItemView.fxml"));
			loader.setController(controller);
			this.stage.setScene(new Scene(loader.load()));
			this.stage.setTitle("Add Rubric Item");
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.stage.showAndWait();
		this.updateRubricTable();
	}

	@FXML
	void warningOkClick(ActionEvent event) {
		this.showRubricWarning(false);
	}

}
