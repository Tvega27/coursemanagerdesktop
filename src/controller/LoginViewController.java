package controller;

import java.io.IOException;
import java.sql.SQLException;

import dal.SQLLoginDal;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Instructor;
import javafx.scene.control.Alert.AlertType;

public class LoginViewController {

	@FXML
	private TextField userBox;

	@FXML
	private PasswordField passwordBox;

	@FXML
	private Button loginBtn;
	
    @FXML
    private Hyperlink firstTimeLoginLink;
	
	private Stage stage;

	private SQLLoginDal loginDal = new SQLLoginDal();
	
	public LoginViewController() {
		this.stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("../view/LoginView.fxml"));
			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("Course Manager Login");
		} catch (IOException e){
			e.printStackTrace();
		}
		
		
	}
	
	public void showStage() {
		this.stage.show();
	}

	@FXML
	void loginClick(ActionEvent event) {
		try {
    		if(this.loginDal.verifyUser(this.userBox.getText(), this.passwordBox.getText())) {
    			this.loadMainStage(this.loginDal.getInstructor());
    			this.userBox.setText("");
        		this.passwordBox.setText("");
    		} else {
    			Alert invalidPasswordAlert = new Alert(AlertType.ERROR, "Invalid username/password.");
        		invalidPasswordAlert.showAndWait();
    		}
		} catch (SQLException sql) {
			sql.printStackTrace();
		}
	}
	
    @FXML
    void firstTimeLoginClicked(ActionEvent event) {
    	this.loadLoginCreationStage();
    }

	private void loadMainStage(Instructor user) {
		MainViewController controller = new MainViewController(user);
		this.stage.close();
		controller.showStage();
	}
	
	private void loadLoginCreationStage() {
		FirstTimeLoginViewController controller = new FirstTimeLoginViewController();
		this.stage.close();
		controller.showStage();
	}

}
