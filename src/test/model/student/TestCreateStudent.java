package test.model.student;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Student;

class TestCreateStudent {

	@Test
	void testGetDegreeProgram() {
		Student student = new Student(0, "Tyler", "Vega", "email@someplace.com", "Computer Science");
		assertEquals("Computer Science", student.getDegreeProgram());
	}
	
	@Test
	void testSetDegreeProgram() {
		Student student = new Student(0, "Tyler", "Vega", "email@someplace.com", "Computer Science");
		student.setDegreeProgram("Biology");
		assertEquals("Biology", student.getDegreeProgram());
	}
	
	@Test
	void testNullDegreeProgram() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Student student = new Student(0, "Tyler", "Vega", "email@someplace.com", null);
			student.getDegreeProgram();
		    });
	}
	
	@Test
	void testNullSetDegreeProgram() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Student student = new Student(0, "Tyler", "Vega", "email@someplace.com", "Computer Science");
			student.setDegreeProgram(null);
		    });
	}

}
