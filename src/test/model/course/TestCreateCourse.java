package test.model.course;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Course;
import model.Instructor;

class TestCreateCourse {
	
	@Test
	void testGetId() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals(0, course.getId());
	}

	@Test
	void testGetCourseNumber() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals("c0001", course.getCourseNumber());
	}
	
	@Test
	void testGetName() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals("cs1", course.getName());
	}
	
	@Test
	void testGetSection() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals("1", course.getSection());
	}
	
	@Test
	void testGetSemester() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals("S19", course.getSemester());
	}
	
	@Test
	void testGetInstructor() {
		Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		Course course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		assertEquals(instructor, course.getInstructor());
	}
	
	@Test
	void testSetNullCourseNumber() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
			Course course = new Course(0, null, "cs1", "1", "S19", instructor);
			course.getCourseNumber();
		    });
	}
	
	@Test
	void testSetNullName() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
			Course course = new Course(0, "c0001", null, "1", "S19", instructor);
			course.getName();
		    });
	}
	
	@Test
	void testSetNullSection() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
			Course course = new Course(0, "c0001", "cs1", "1", null, instructor);
			course.getSection();
		    });
	}
	
	@Test
	void testSetNullSemester() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Instructor instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
			Course course = new Course(0, "c0001", "cs1", "1", null, instructor);
			course.getSemester();
		    });
	}
	
	@Test
	void testSetNullInstructor() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			Course course = new Course(0, "c0001", "cs1", "1", "S19", null);
			course.getInstructor();
		    });
	}
}
