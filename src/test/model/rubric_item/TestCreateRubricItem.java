package test.model.rubric_item;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import model.RubricItem;

class TestCreateRubricItem {
	
	
	@Test
	void testGetId() {
		RubricItem item = new RubricItem(1, "Title", 0.15);
		
		assertEquals(1, item.getId());
	}
	
	@Test
	void testSetId() {
		RubricItem item = new RubricItem(1, "Title", 0.15);
		item.setId(2);
		
		assertEquals(2, item.getId());
	}
	@Test
	void testGetTitle() {
		RubricItem item = new RubricItem(1, "Title", 0.15);
		assertEquals("Title", item.getTitle());
	}
	
	@Test
	void testGetWeight() {
		RubricItem item = new RubricItem(1, "Title", 0.15);
		assertEquals(0.15, item.getWeight());
	}
	
	@Test
	void testSetNullTitle() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			RubricItem item = new RubricItem(1, null, 0.15);
			item.getTitle();
		    });
	}
	
	@Test
	void testSetWeightUnder0() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			RubricItem item = new RubricItem(1, "Title", -0.1);
			item.getWeight();
		    });
	}
	
	@Test
	void testSetWeightOver1() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			RubricItem item = new RubricItem(1, "Title", 1.1);
			item.getWeight();
		    });
	}

}
