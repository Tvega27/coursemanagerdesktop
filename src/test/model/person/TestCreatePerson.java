package test.model.person;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Person;

class TestCreatePerson {

	@Test
	void testGetFirstName() {
		Person person = new Person(0, "Tyler", "Vega", "email@someplace.com");
		assertEquals("Tyler", person.getFirstName());
	}
	
	@Test
	void testGetLastName() {
		Person person = new Person(0, "Tyler", "Vega", "email@someplace.com");
		assertEquals("Vega", person.getLastName());
	}
	
	@Test
	void testGetEmail() {
		Person person = new Person(0, "Tyler", "Vega", "email@someplace.com");
		assertEquals("email@someplace.com", person.getEmail());
	}
	
	@Test
	void testGetId() {
		Person person = new Person(1, "Tyler", "Vega", "email@someplace.com");
		assertEquals(1, person.getId());
	}

}
