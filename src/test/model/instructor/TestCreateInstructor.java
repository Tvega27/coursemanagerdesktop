package test.model.instructor;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Instructor;

class TestCreateInstructor {

	@Test
	void testGetPublicEmail() {
		Instructor instructor = new Instructor(0, "Tyler", "Vega", "email@someplace.com", "publicemail@someplace.com");
		assertEquals("publicemail@someplace.com", instructor.getPublicEmail());
	}
	
	@Test
	void testSetPublicEmail() {
		Instructor instructor = new Instructor(0, "Tyler", "Vega", "email@someplace.com", "publicemail@someplace.com");
		instructor.setPublicEmail("newemail@someplace.com");
		assertEquals("newemail@someplace.com", instructor.getPublicEmail());
	}
}
