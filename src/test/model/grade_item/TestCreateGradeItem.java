package test.model.grade_item;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import model.Course;
import model.GradeItem;
import model.Instructor;
import model.Student;

class TestCreateGradeItem {
	private Instructor instructor;
	private Course course;
	private Student student;

	@BeforeEach
	void setUp() throws Exception {
		this.instructor = new Instructor(0, "Jonathan", "Corley", "email@someplace.com", "publicemail@someplace.com");
		this.course = new Course(0, "c0001", "cs1", "1", "S19", instructor);
		this.student = new Student(0, "Tyler", "Vega", "email1@someplace.com", "CS");
	}
	
	@Test
	void testGetId() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals(0, item.getId());
	}

	@Test
	void testGetCategory() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals("Assignment", item.getCategory());
	}
	
	@Test
	void testGetTitle() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals("A1", item.getTitle());
	}
	
	@Test
	void testGetDescription() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals("S19", item.getDescription());
	}
	
	@Test
	void testGetStudent() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals(this.student, item.getStudent());
	}
	
	@Test
	void testGetGrade() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals(100.0, item.getGrade());
	}
	
	@Test
	void testGetCourse() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals(this.course, item.getCourse());
	}
	
	@Test
	void testGetMaxGrade() {
		GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, this.course, 100);
		assertEquals(100, item.getMaxGrade());
	}
	
	@Test
	void testSetNullCategory() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, null, "A1", "S19", this.student, 100.0, this.course, 100);
			item.getCategory();
		    });
	}
	
	@Test
	void testSetNullTitle() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", null, "S19", this.student, 100.0, this.course, 100);
			item.getTitle();
		    });
	}
	
	@Test
	void testSetNullDescription() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", "A1", null, this.student, 100.0, this.course, 100);
			item.getDescription();
		    });
	}
	
	@Test
	void testSetNullStudent() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", null, 100.0, this.course, 100);
			item.getStudent();
		    });
	}
	
	@Test
	void testSetNegativeGrade() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, -1.0, this.course, 100);
			item.getGrade();
		    });
	}
	
	@Test
	void testSetNullCourse() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, 100.0, null, 100);
			item.getCourse();
		    });
	}
	
	@Test
	void testSetNegativeMaxGrade() {
		
		assertThrows(IllegalArgumentException.class, () -> {
			GradeItem item = new GradeItem(0, "Assignment", "A1", "S19", this.student, -1.0, this.course, -10);
			item.getMaxGrade();
		    });
	}

}
