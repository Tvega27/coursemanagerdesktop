package dal;

import java.sql.SQLException;
import java.util.ArrayList;

import model.Course;
import model.RubricItem;

/**
 * Interface for the RubricItem Dal.
 * 
 * @author Tyler Vega
 * @version 2/15/2019
 */
public interface RubricItemDal {

	/**
	 * Returns the rubric items for the specified rubric.
	 * 
	 * @param rubric the rubric.
	 * 
	 * @return The rubric items for the rubric.
	 */
	ArrayList<RubricItem> getRubricItemsByRubric(Course course) throws SQLException;
	
	/**
	 * Updates the rubric items for the specified rubric.
	 * 
	 * @param rubricItem the rubricItem being updated
	 * 
	 * @postcondition the rubricItem is updated to the given values
	 */
	void updateRubricItem(RubricItem rubricItem) throws SQLException;
	
	/**
	 * Adds a new rubric item to the database
	 * 
	 * @param rubricItem the rubricItem being added
	 * 
	 * @throws SQLException if there is a problem connecting to the db
	 * 
	 * @postcondition the rubricItem is added to the database
	 */
	void addRubricItem(RubricItem rubricItem) throws SQLException;
	
	/**
	 * Deletes the specified rubricItem from the database
	 * 
	 * @param rubricItem the rubricItem being deleted
	 * 
	 * @throws SQLException if there is a problem connecting to the db
	 * 
	 * @postcondition the rubricItem is deleted from the database
	 * */
	void deleteRubricItem(RubricItem rubricItem) throws SQLException;
}
