package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Course;
import model.RubricItem;

/**
 * Sqlite Dal for the RubricItem.
 * 
 * @author Tyler Vega
 * @version 2/15/2019
 */
public class SQLRubricItemDal implements RubricItemDal {

	@Override
	public ArrayList<RubricItem> getRubricItemsByRubric(Course course) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT ri.id, ri.title, ri.weight "
				+ "FROM gradebook_course c, gradebook_rubricitem ri, gradebook_rubric r, gradebook_person p "
				+ "WHERE r.course_id = c.id " + "AND ri.rubric_id = r.course_id " + "AND p.first_name = ? "
				+ "AND p.last_name = ?" + "AND c.course_number = ?";

		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, course.getInstructor().getFirstName());
		statement.setString(2, course.getInstructor().getLastName());
		statement.setString(3, course.getCourseNumber());

		ResultSet rs = statement.executeQuery();

		ArrayList<RubricItem> rubricItems = new ArrayList<RubricItem>();

		while (rs.next()) {
			int id = rs.getInt("id");
			String title = rs.getString("title");
			double weight = rs.getDouble("weight");

			rubricItems.add(new RubricItem(id, title, weight));
		}
		con.close();
		return rubricItems;
	}

	@Override
	public void updateRubricItem(RubricItem rubricItem) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "UPDATE gradebook_rubricitem " + "SET title = ?, " + "weight = ? " + "WHERE id = ?";

		PreparedStatement statement = con.prepareStatement(query);

		statement.setString(1, rubricItem.getTitle());
		statement.setDouble(2, rubricItem.getWeight());
		statement.setInt(3, rubricItem.getId());

		statement.executeUpdate();
	}

	@Override
	public void addRubricItem(RubricItem rubricItem) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "INSERT INTO gradebook_rubricitem (title, weight, rubric_id) " + "VALUES (?, ?, ?) ";

		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, rubricItem.getTitle());
		statement.setDouble(2, rubricItem.getWeight());
		statement.setInt(3, rubricItem.getId());

		statement.executeUpdate();
	}

	@Override
	public void deleteRubricItem(RubricItem rubricItem) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "DELETE FROM gradebook_rubricitem "
				+ "WHERE id = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setInt(1, rubricItem.getId());
		
		statement.executeUpdate();

	}

}
