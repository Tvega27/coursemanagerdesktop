package dal;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;

import model.Course;
import model.GradeItem;
import model.Student;

/**
 * Interface for the GradeItem Dal.
 * 
 * @author Tyler Vega
 * @version 2/17/2019
 */
public interface GradeItemDal {

	/**
	 * Returns the grade items for the specified category and assignment.
	 * 
	 * @param course     the course of the GradeItem.
	 * 
	 * @param category   the category of the GradeItem.
	 * 
	 * @param assignment the assignment name for the GradeItem.
	 * 
	 * @return The grade items of specified category and assignment.
	 * 
	 * @throws SQLException
	 */
	ArrayList<GradeItem> getGradeItemsByCategoryAssignment(Course course, String category, String assignment)
			throws SQLException;

	/**
	 * Returns a tree map of the GradeItem categories by course. The Key is the
	 * category, and the Value is a list of the assignments in that category.
	 * 
	 * @param course The course
	 * 
	 * @return A tree map of the GradeItem's Categories with their assignments.
	 * 
	 * @throws SQLException
	 */
	TreeMap<String, ArrayList<String>> getCategoriesByCourse(Course course) throws SQLException;

	/**
	 * Returns a list of the categories. The categories are the same as the rubric
	 * items set in the course.
	 * 
	 * @param course The course
	 * 
	 * @return A list of the categories for the course.
	 * @throws SQLException
	 */
	ArrayList<String> getCategories(Course course) throws SQLException;

	/**
	 * Returns a list of the assignment titles by specified category.
	 * 
	 * @param course   The course.
	 * @param category The category the assignment is in.
	 * 
	 * @return A list of the assignment titles.
	 * 
	 * @throws SQLException
	 */
	ArrayList<String> getAssignmentTitles(Course course, String category) throws SQLException;

	/**
	 * Adds a new GradeItem to the database.
	 * 
	 * @precondition item != null
	 * 
	 * @param item The grade item to add.
	 * 
	 * @postcondition The grade item is added to the database, or an exception is
	 *                thrown.
	 * 
	 * @throws SQLException
	 */
	void addGradeItem(GradeItem item) throws SQLException;

	/**
	 * Updates the existing GradeItem in the database.
	 * 
	 * @precondition item != null
	 * 
	 * @param item The grade item to update.
	 * 
	 * @postcondition The existing grade item is updated.
	 * 
	 * @throws SQLException
	 */
	void updateGradeItem(GradeItem item) throws SQLException;

	/**
	 * Deletes the specified GradeItem from the database.
	 * 
	 * @param courseId The course id.
	 * @param category The GradeItem category
	 * @param title    The GradeItem title
	 * 
	 * @throws SQLException
	 * 
	 * @postcondition The GradeItem is deleted from the database.
	 */
	void deleteGradeItem(int courseId, String category, String title) throws SQLException;
	
	ArrayList<GradeItem> getGradeItemsForStudentInCourse(Course course, Student student) throws SQLException;
	
	void updateGradeItemGrade(GradeItem item) throws SQLException;

}
