package dal;

import java.sql.SQLException;

/**
 * Interface for logging a user into a data source.
 * 
 * @author Tyler Vega, David Jarrett
 * @version 3/2/2019
 */
public interface LoginDal {

	/**
	 * Ensures the the username/password combination is valid.
	 * 
	 * @preconditions: None
	 * @postconditions: getUserFirstName(), getUserLastName(), and getUserID()
	 *                  should be callable.
	 * @param username The username for the data source.
	 * @param password The password for the data source.
	 * @return true if the user was successfully logged in, false otherwise.
	 */
	boolean verifyUser(String username, String password) throws SQLException;

	void setPassword(String username, String password) throws SQLException;

	/**
	 * Checks the database if the username is an authorized username for desktop
	 * password creation.
	 * 
	 * @param username the username
	 * 
	 * @return true if the username is an authorized username
	 * 
	 * @throws SQLException
	 */
	boolean checkAuthUsername(String username) throws SQLException;

}
