package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.TreeMap;

import model.Course;
import model.GradeItem;
import model.Student;

/**
 * Sqlite Dal for the GradeItem.
 * 
 * @author Tyler Vega
 * @version 2/17/2019
 */
public class SQLGradeItemDal implements GradeItemDal {

	@Override
	public ArrayList<GradeItem> getGradeItemsByCategoryAssignment(Course course, String category, String assignment) throws SQLException{
		ArrayList<GradeItem> items = new ArrayList<GradeItem>();
		
		var con = DbConnection.getConnection();
		String query = "SELECT gi.id as gid, gi.title, gi.grade, gi.description, gi.max_grade, dp.name, p.id as pid, p.first_name, p.last_name, p.email " +
					   "FROM gradebook_course c, gradebook_person p, gradebook_gradeitem gi, gradebook_student s, gradebook_degreeprogram dp " +
					   "WHERE gi.student_id =  s.person_ptr_id "
					   + "AND s.person_ptr_id = p.id "
					   + "AND s.degree_program_id = dp.id "
					   + "AND gi.course_id = c.id "
					   + "AND c.course_number = ? "
					   + "AND gi.category = ? "
					   + "AND gi.title = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		
		statement.setString(1, course.getCourseNumber());
		statement.setString(2, category);
		statement.setString(3, assignment);
		
		ResultSet rs = statement.executeQuery();
		
		while(rs.next()) {
			int gradeItemId = rs.getInt("gid");
			String title = rs.getString("title");
			double grade = rs.getDouble("grade");
			String description = rs.getString("description");
			double maxGrade = rs.getDouble("max_grade");
			String degreeProgram = rs.getString("name");
			int personId = rs.getInt("pid");
			String firstName = rs.getString("first_name");
			String lastName = rs.getString("last_name");
			String email = rs.getString("email");
			
			Student student = new Student(personId, firstName, lastName, email, degreeProgram);
			items.add(new GradeItem(gradeItemId, category, title, description, student, grade, course, maxGrade));
			
		}
		con.close();
		return items;
	}

	@Override
	public TreeMap<String, ArrayList<String>> getCategoriesByCourse(Course course) throws SQLException {
		TreeMap<String, ArrayList<String>> categories = new TreeMap<String, ArrayList<String>>();
		var con = DbConnection.getConnection();
		String query = "SELECT ri.title, gi.title " +
					   "FROM gradebook_course c, gradebook_gradeitem gi, gradebook_semester s , gradebook_instructor i, gradebook_rubricitem ri " +
					   "WHERE gi.course_id = c.id "
					   + "AND ri.rubric_id = c.id "
					   + "AND gi.course_id = ? "
					   + "AND i.person_ptr_id = ? "
					   + "AND c.course_number = ? "
					   + "AND c.semester_id = s.id "
					   + "AND s.name = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		
		statement.setInt(1, course.getId());
		statement.setInt(2, course.getInstructor().getId());
		statement.setString(3, course.getCourseNumber());
		statement.setString(4, course.getSemester());
		
		ResultSet rs = statement.executeQuery();
		while(rs.next()) {
			String category = rs.getString("ri.title");
			String title = rs.getString("gi.title");
			
			ArrayList<String> list = categories.get(category);
			if(list == null) {
		         list = new ArrayList<String>();
		         list.add(title);
		         categories.put(category, list);
		    } else {
		        if(!list.contains(title)) {
		        	list.add(title);
		        }
		    }
		}
		con.close();
		
		return categories;
	}

	@Override
	public void addGradeItem(GradeItem item) throws SQLException {
		
		var con = DbConnection.getConnection();
		String query = "INSERT INTO gradebook_gradeitem (category, title, description, grade, course_id, student_id, max_grade) " +
					   "VALUES (?, ?, ?, ?, ?, ?, ?) ";
		
		PreparedStatement statement = con.prepareStatement(query);
		
		statement.setString(1, item.getCategory());
		statement.setString(2, item.getTitle());
		statement.setString(3, item.getDescription());
		statement.setDouble(4, item.getGrade());
		statement.setInt(5, item.getCourse().getId());
		statement.setInt(6, item.getStudent().getId());
		statement.setDouble(7, item.getMaxGrade());
		
		statement.executeUpdate();
	}

	@Override
	public void updateGradeItem(GradeItem item) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "UPDATE gradebook_gradeitem " +
					   "SET category = ?, "
					   + "title = ?, "
					   + "description = ?, "
					   + "grade = ?, "
					   + "max_grade = ? "
					   + "WHERE course_id = ?  "
					   + "AND student_id = ?";
					   
		
		PreparedStatement statement = con.prepareStatement(query);
		
		statement.setString(1, item.getCategory());
		statement.setString(2, item.getTitle());
		statement.setString(3, item.getDescription());
		statement.setDouble(4, item.getGrade());
		statement.setDouble(5, item.getMaxGrade());
		statement.setInt(6, item.getCourse().getId());
		statement.setInt(7, item.getStudent().getId());
		
		statement.executeUpdate();
		
	}

	@Override
	public void updateGradeItemGrade(GradeItem item) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "UPDATE gradebook_gradeitem " +
					   "SET grade = ? "
					   + "WHERE course_id = ?  "
					   + "AND student_id = ? "
					   + "AND title = ? "
					   + "AND category = ?";
					   
		
		PreparedStatement statement = con.prepareStatement(query);
		
		statement.setDouble(1, item.getGrade());
		statement.setInt(2, item.getCourse().getId());
		statement.setInt(3, item.getStudent().getId());
		statement.setString(4, item.getTitle());
		statement.setString(5, item.getCategory());
		
		statement.executeUpdate();
		
	}
	@Override
	public void deleteGradeItem(int courseId, String category, String title) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "DELETE FROM gradebook_gradeitem "
				+ "WHERE course_id = ? "
				+ "AND category = ? "
				+ "AND title = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setInt(1, courseId);
		statement.setString(2, category);
		statement.setString(3, title);
		
		statement.executeUpdate();
		
	}

	@Override
	public ArrayList<String> getCategories(Course course) throws SQLException {
		var con = DbConnection.getConnection();
		ArrayList<String> categories = new ArrayList<String>();
		
		String query = "SELECT ri.title " +
				   "FROM gradebook_course c, gradebook_semester s , gradebook_instructor i, gradebook_rubricitem ri " +
				   "WHERE ri.rubric_id = c.id "
				   + "AND i.person_ptr_id = ? "
				   + "AND c.course_number = ? "
				   + "AND c.semester_id = s.id "
				   + "AND s.name = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setInt(1, course.getInstructor().getId());
		statement.setString(2, course.getCourseNumber());
		statement.setString(3, course.getSemester());
		
		ResultSet rs = statement.executeQuery();
		while(rs.next()) {
			String category = rs.getString("ri.title");
			categories.add(category);
		}
		return categories;
	}

	@Override
	public ArrayList<String> getAssignmentTitles(Course course, String category) throws SQLException {
		var con = DbConnection.getConnection();
		ArrayList<String> titles = new ArrayList<String>();
		
		String query = "SELECT gi.title " +
				   "FROM gradebook_course c, gradebook_semester s, gradebook_gradeitem gi " +
				   "WHERE gi.course_id = c.id "
				   + "AND c.course_number = ? "
				   + "AND c.semester_id = s.id "
				   + "AND s.name = ? "
				   + "AND gi.category = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, course.getCourseNumber());
		statement.setString(2, course.getSemester());
		statement.setString(3, category);
		
		ResultSet rs = statement.executeQuery();
		
		while(rs.next()) {
			String title = rs.getString("gi.title");
			if(!titles.contains(title)) {
				titles.add(title);
			}
		}
		
		return titles;
	}

	@Override
	public ArrayList<GradeItem> getGradeItemsForStudentInCourse(Course course, Student student) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT gi.id, gi.category, gi.title, gi.grade, gi.description, gi.max_grade " +
				   "FROM gradebook_course c, gradebook_semester s, gradebook_gradeitem gi, gradebook_student st " +
				   "WHERE gi.course_id = c.id "
				   + "AND c.course_number = ? "
				   + "AND c.semester_id = s.id "
				   + "AND s.name = ? "
				   + "AND st.person_ptr_id = gi.student_id "
				   + "AND st.person_ptr_id = ?";
		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, course.getCourseNumber());
		statement.setString(2, course.getSemester());
		statement.setInt(3, student.getId());
		
		ResultSet rs = statement.executeQuery();
		ArrayList<GradeItem> items = new ArrayList<GradeItem>();
		while(rs.next()) {
			int gradeItemId = rs.getInt("gi.id");
			String category = rs.getString("category");
			String title = rs.getString("title");
			double grade = rs.getDouble("grade");
			String description = rs.getString("description");
			double maxGrade = rs.getDouble("max_grade");
			
			items.add(new GradeItem(gradeItemId, category, title, description, student, grade, course, maxGrade));
		}
		return items;
	}
	
	
	
	

}
