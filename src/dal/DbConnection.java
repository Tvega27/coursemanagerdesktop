package dal;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Defines a Database connection to the MySQL database.
 * 
 * @author Tyler Vega
 * @version 2/12/2019
 */
public class DbConnection {
	/**
	 * Gets a db Connection
	 * 
	 * @return a Connection object to the DB
	 */
	public static Connection getConnection() {
		String connectionString = "jdbc:mysql://159.65.164.234:3306/course_manager_db?useSSL=false&user=remote_user&password=itstricky&serverTimezone=EST&allowMultiQueries=true";
		Connection con = null;
		try {
			con = DriverManager.getConnection(connectionString); 
			System.out.println("Connection to SQLite has been established.");
		} catch (SQLException ex) {
			System.out.println("SQLException: " + ex.getMessage());
			System.out.println("SQLState: " + ex.getSQLState());
			System.out.println("VendorError: " + ex.getErrorCode());
		} catch (Exception e) {
            System.out.println(e.toString());
        }
		return con;
      }
}