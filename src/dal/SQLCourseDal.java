package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Course;
import model.Instructor;
import model.Student;

public class SQLCourseDal implements CourseDal {

	@Override
	public ArrayList<Course> getCoursesByInstructor(Instructor instructor) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT c.id, course_number, c.name, section, s.name " +
					   "FROM gradebook_course c, gradebook_instructor i, gradebook_person p, gradebook_semester s " +
					   "WHERE p.first_name = ? " +
					   "AND p.last_name = ? " +
					   "AND i.person_ptr_id = p.id " +
					   "AND  c.instructor_id = i.person_ptr_id";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, instructor.getFirstName());
		statement.setString(2, instructor.getLastName());
		
		ResultSet rs = statement.executeQuery();
		
		ArrayList<Course> courses = new ArrayList<Course>();
		
		while(rs.next()) {
			int id = rs.getInt("id");
			String courseNumber = rs.getString("course_number");
			String name = rs.getString("name");
			String section = rs.getString("section");
			String semester = rs.getString("s.name");
			
			courses.add(new Course(id, courseNumber, name, section, semester, instructor));
		}
		con.close();
		return courses;
	}

	@Override
	public ArrayList<Student> getStudentsInCourse(Course course) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT * " +
					   "FROM gradebook_takingcourse tc, gradebook_person p, gradebook_student s, gradebook_degreeprogram dp "
					   + "WHERE tc.course_id = ? "
					   + "AND s.person_ptr_id = p.id "
					   + "AND tc.student_id = s.person_ptr_id "
					   + "AND s.degree_program_id = dp.id";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setInt(1, course.getId());
		
		ResultSet rs = statement.executeQuery();
		ArrayList<Student> roster = new ArrayList<Student>();
		
		while(rs.next()) {
			int id = rs.getInt("student_id");
			String firstName = rs.getString("first_name");
			String lastName = rs.getString("last_name");
			String email = rs.getString("email");
			String degree = rs.getString("name");
			
			roster.add(new Student(id, firstName, lastName, email, degree));
		}
		return roster;
	}

}
