package dal;

import java.sql.SQLException;
import java.util.ArrayList;

import model.Course;
import model.Instructor;
import model.Student;

public interface CourseDal {

	/**
	 * Returns the courses that the specified instructor teaches.
	 * 
	 * @param instructor The instructor.
	 * 
	 * @return A list of the courses that the instructor teaches.
	 * 
	 * @throws SQLException
	 */
	ArrayList<Course> getCoursesByInstructor(Instructor instructor) throws SQLException;

	/**
	 * Returns the class roster for a course.
	 * 
	 * @param course The course.
	 * 
	 * @return The class roster for a course.
	 * @throws SQLException
	 */
	ArrayList<Student> getStudentsInCourse(Course course) throws SQLException;
}
