package dal;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

import model.Instructor;

public class SQLLoginDal implements LoginDal {

	private static final String loginQuery = "SELECT du.auth_username, du.password as dpw, p.id, u.first_name, u.last_name, p.email, i.public_email "
			+ "FROM desktop_user_login du, auth_user u, gradebook_instructor i, gradebook_person p "
			+ "WHERE du.auth_username = u.username " + "AND p.first_name = u.first_name " + "AND p.last_name = u.last_name "
			+ "AND p.id = i.person_ptr_id "
			+ "AND auth_username = ?";

	private Instructor instructor;

	@Override
	public boolean verifyUser(String username, String password) throws SQLException {
		try (var con = DbConnection.getConnection()) {
			var query = prepareQuery(username, password, con);
			var result = query.executeQuery();
			if (result.next() && this.verifyPassword(result, password)) {
				setUserInformation(result);
				return true;
			}
			return false;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean verifyPassword(ResultSet result, String password) throws SQLException {
		var hash = result.getBytes("dpw");
		MessageDigest md;
		try {
			md = MessageDigest.getInstance("SHA-512");
			byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
			return Arrays.equals(hash, hashedPassword);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}

	private PreparedStatement prepareQuery(String username, String password, Connection con) throws SQLException {
		var query = con.prepareStatement(loginQuery);
		query.setString(1, username);
		// query.setString(2, password);
		return query;
	}

	private void setUserInformation(ResultSet result) throws SQLException {
		int userID = result.getInt(3);
		String userFirstName = result.getString(4);
		String userLastName = result.getString(5);
		String email = result.getString(6);
		String publicEmail = result.getString(7);

		this.instructor = new Instructor(userID, userFirstName, userLastName, email, publicEmail);

	}

	/**
	 * Returns the Instructor of the login.
	 * 
	 * @return The Instructor that logged in.
	 */
	public Instructor getInstructor() {
		return this.instructor;
	}
	
	@Override
	public void setPassword(String username, String password) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "INSERT INTO desktop_user_login(auth_username, password) VALUES (?, ?)";
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-512");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		byte[] hashedPassword = md.digest(password.getBytes(StandardCharsets.UTF_8));
		
		PreparedStatement p = con.prepareStatement(query);
		
		p.setString(1, username);
		p.setBytes(2, hashedPassword);
		
		
		p.executeUpdate();
	}

	@Override
	public boolean checkAuthUsername(String username) throws SQLException {
		var con = DbConnection.getConnection();
		String query = "SELECT username FROM auth_user WHERE username = ?";
		
		PreparedStatement statement = con.prepareStatement(query);
		statement.setString(1, username);
		
		ResultSet rs = statement.executeQuery();
		String check = "";
		while(rs.next()) {
			check = rs.getString("username");
		}
		return !check.equals("");
	}
	
	

}
